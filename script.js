// console.log(`Hello world`);

let number = Number(prompt(`Please enter a number: `));
console.log(`The number you provided is ${number}`);

for (number; number >= 0; number -= 5) {
  if (number <= 50) {
    console.log(`The current value is at ${number}. Terminating the loop.`);
    break;
  } else if (number % 10 === 0) {
    console.log(`The number is divisible by 10. Skipping number.`);
  } else if (number % 5 === 0) {
    console.log(number);
  }
}

let longWord = "supercalifragilisticexpialidocious";
let consonants = "";

for (let i = 0; i < longWord.length; i++) {
  if (
    longWord[i].toLowerCase() === "a" ||
    longWord[i].toLowerCase() === "e" ||
    longWord[i].toLowerCase() === "i" ||
    longWord[i].toLowerCase() === "o" ||
    longWord[i].toLowerCase() === "u"
  ) {
    continue;
  } else {
    consonants += longWord[i];
  }
}

console.log(consonants);
